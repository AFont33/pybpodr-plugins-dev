# !/usr/bin/python3
# -*- coding: utf-8 -*-

import water_calibration_plugin.settings as settings
import numpy as np

class DefaultValues:

    def __init__(self, gui):
        self.gui = gui
        self.val = {}
        try:
            self.__loadFromFile()
        except FileNotFoundError:
            self.__loadAllDefaultVariables()
            pass
        except Exception as err:
            print(err)

    def getValues(self):
        """ Return the default variables """
        return self.val

    def save(self):
        """ Save all the variable in a file """
        self.__recordVariables()
        np.save(settings.WATER_PLUGIN_PATH_DEFAULT, self.val)


    def __loadFromFile(self):
        self.val = np.load(settings.WATER_PLUGIN_PATH_DEFAULT)
        self.val = self.val.item()

    def __loadAllDefaultVariables(self):
        """ Set the default variables """
        self.val['_microLneeds'] = ['1', '1', '1', '1', '1', '1', '1', '1']
        self.val['_firstTimes']  = ['0.1', '0.1', '0.1', '0.1', '0.1', '0.1', '0.1', '0.1']
        self.val['_ports']       = [False, False, False, False, False, False, False, False]
        self.val['_cyles']       = '10'
        self.val['_tolerance']   = '0.3'
        self.val['_interval']    = '1'
        self.val['_board']       = None

    def __recordVariables(self):
        """ Record all the setting parameters """
        vTemp = []
        for form in self.gui._allmicroLneeds:
            vTemp.append(form.value)
        self.val['_microLneeds']   = vTemp

        vTemp = []
        for form in self.gui._allfirstTimes:
            vTemp.append(form.value)
        self.val['_firstTimeP1'] = vTemp

        vTemp = []
        for form in self.gui._allports:
            vTemp.append(form.value)
        self.val['_ports'] = vTemp

        self.val['_cyles']       = self.gui._cyles.value
        self.val['_tolerance']   = self.gui._tolerance.value
        self.val['_interval']    = self.gui._interval.value
        self.val['_board']       = self.gui._board.text



