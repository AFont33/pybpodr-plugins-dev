# !/usr/bin/python3
# -*- coding: utf-8 -*-
import cffi, logging
import mccdaqR, numpy as np, matplotlib.pyplot as plt


class DaqR_usb1608G:
    def __init__(self):
        # ---------------------------------------------------------------------------------------------------------------------
        logging.basicConfig(level=logging.WARNING)
        logging.getLogger(__name__)

        VENDOR_ID = 0x09db
        PRODUCT_ID = 0x0135  # USB-1608GX

        self.ffi = cffi.FFI()

        # Setup for libusb calls
        self.ffi.cdef("""
        struct libusb_device_handle;
        typedef struct libusb_device_handle libusb_device_handle;

        struct libusb_context;
        typedef struct libusb_context libusb_context;

        int libusb_init(libusb_context **ctx);
        libusb_device_handle * libusb_open_device_with_vid_pid(
                libusb_context *ctx, uint16_t vendor_id, uint16_t product_id);
        int libusb_bulk_transfer(libusb_device_handle *dev_handle,
                unsigned char endpoint, unsigned char *data, int length,
                int *actual_length, unsigned int timeout);
        """)

        logging.info('Try to open libusb')
        libusb = self.ffi.dlopen("libusb-1.0.so")

        header = mccdaqR.__path__[0] + '/utils/usb1608GX.h'
        cdef_from_file = None
        try:
            with open(header, 'r') as libusb_header:
                cdef_from_file = libusb_header.read()
        except FileNotFoundError:
            print('Unable to find "%s"' % header)
            exit(1)
        except IOError:
            print('Unable to open "%s"' % header)
            exit(2)
        finally:
            if cdef_from_file == '':
                print('File "%s" is empty' % header)
                exit(3)

        # Setup for libmccusb, specifically usb-1608G calls
        self.ffi.cdef(cdef_from_file)

        # Setup for libmccusb, specifically pmd.c calls
        self.ffi.cdef("""
        libusb_device_handle* usb_device_find_USB_MCC( int productId, char *serialID );
        int usb_get_max_packet_size(libusb_device_handle* udev, int endpointNum);
        int getUsbSerialNumber(libusb_device_handle *udev, unsigned char serial[]);
        int sendStringRequest(libusb_device_handle *udev, char *message);
        int  getStringReturn(libusb_device_handle *udev, char *message);
        """)

        logging.info('try to open libmccusb')
        self.mcclib = self.ffi.dlopen("libmccusb.so", self.ffi.RTLD_LAZY)  # http://man7.org/linux/man-pages/man3/dlopen.3.html

        logging.debug('Trying to initialize USB-CTR')
        ret = libusb.libusb_init(self.ffi.NULL)
        logging.debug('ret from libusb_init %d' % ret)
        logging.debug('find usb-ctr8')
        serialID = self.ffi.new("char[]", b"01C79E9B")  # serial ID of usb-1608GX device
        # ---------------------------------------------------------------------------------------------------------------------

        self.dev = self.mcclib.usb_device_find_USB_MCC(PRODUCT_ID, serialID)
        logging.debug('Found device: %r, try to init' % self.dev)
        if self.dev == self.ffi.NULL:
            msg = 'Could not find the device, unplug / replug in device'
            raise ValueError(msg)

        self.mcclib.usbInit_1608G(self.dev, 2)
        logging.debug('Finished init usb-CTR')

        self.table_AIN = self.ffi.new('float table_AIN[NGAINS_1608G][2]')
        self.table_AO = self.ffi.new('float table_AO[NCHAN_AO_1608GX][2]')
        self.mcclib.usbBuildGainTable_USB1608G(self.dev, self.table_AIN)
        self.mcclib.usbBuildGainTable_USB1608GX_2AO(self.dev, self.table_AO)

    def scanStream(self, channel = 0, g = 10, m = 0, nLoop = 20000):
        # Clean the device
        self.mcclib.usbAInScanStop_USB1608G(self.dev)
        self.mcclib.usbAInScanClearFIFO_USB1608G(self.dev)

        gain = self.mcclib.BP_10V
        mode = m

        # Mode settings
        if m == self.mcclib.SINGLE_ENDED:
            mode = self.mcclib.SINGLE_ENDED #Input channel [0-15]
            if channel > 15:
                raise ValueError('Mode Single ended selected. The channel has to be between 0 and 15')
            logging.debug('Mode Single ended selected.')
        else:
            mode = self.mcclib.DIFFERENTIAL #Input channel [0-7]
            if channel > 7:
                raise ValueError('Mode Differential selected. The channel has to be between 0 and 7')
            logging.debug('Mode Differential selected.')

        # Gain settings
        if g == 10:
            gain = self.mcclib.BP_10V
        elif g == 5:
            gain = self.mcclib.BP_5V
        elif g == 2:
            gain = self.mcclib.BP_2V
        elif g == 1:
            gain = self.mcclib.BP_1V
        else:
            raise ValueError('The gain has to be one of the follows: 1, 2, 5, 10 (V).')

        # Configuration
        mode |= self.mcclib.LAST_CHANNEL
        list = self.ffi.new('ScanList[16]')
        list[0].range = gain
        list[0].mode = mode
        list[0].channel = channel
        self.mcclib.usbAInConfig_USB1608G(self.dev, list)

        vecOut = np.zeros(nLoop)
        for i in range(nLoop):
            value = self.mcclib.usbAIn_USB1608G(self.dev, channel)
            value = np.rint(value * self.table_AIN[gain][0] + self.table_AIN[gain][1])
            #print('sample:', value, 'volt:',self.mcclib.volts_USB1608G(gain, np.uint16(value)))
            vecOut[i] = self.mcclib.volts_USB1608G(gain, np.uint16(value))
            #time.sleep(0.1)

        return vecOut

    def scan(self, channel=0, g=10, m=0, timeScan=1, frequency=20000):
        # Clean the device
        self.mcclib.usbAInScanStop_USB1608G(self.dev)
        self.mcclib.usbAInScanClearFIFO_USB1608G(self.dev)

        mode = self.mcclib.SINGLE_ENDED
        nchan = 1
        gain = self.mcclib.BP_10V
        nScans = timeScan*frequency

        # Mode settings
        if m == self.mcclib.SINGLE_ENDED:
            mode = self.mcclib.SINGLE_ENDED #Input channel [0-15]
            if channel > 15:
                raise ValueError('Mode Single ended selected. The channel has to be between 0 and 15')
            logging.debug('Mode Single ended selected.')
        else:
            mode = self.mcclib.DIFFERENTIAL #Input channel [0-7]
            if channel > 7:
                raise ValueError('Mode Differential selected. The channel has to be between 0 and 7')
            logging.debug('Mode Differential selected.')

        # Gain settings
        if g == 10:
            gain = self.mcclib.BP_10V
        elif g == 5:
            gain = self.mcclib.BP_5V
        elif g == 2:
            gain = self.mcclib.BP_2V
        elif g == 1:
            gain = self.mcclib.BP_1V
        else:
            raise ValueError('The gain has to be one of the follows: 1, 2, 5, 10 (V).')

        # Configuration
        mode |= self.mcclib.LAST_CHANNEL
        list = self.ffi.new('ScanList[16]')
        list[0].range = gain
        list[0].mode = mode
        list[0].channel = channel
        self.mcclib.usbAInConfig_USB1608G(self.dev, list)

        # Create a data vector and a pointer for it
        vecIn = np.zeros(nchan*nScans, dtype='uint16')
        sdataIn = self.ffi.cast('uint16_t *', vecIn.ctypes.data)

        # Start record
        self.mcclib.usbAInScanStart_USB1608G(self.dev, nScans, 0, frequency, 0)
        ret = self.mcclib.usbAInScanRead_USB1608G(self.dev, nScans, nchan, sdataIn, 20000, 0)

        logging.debug('Converting data.')
        vecOut = np.zeros(len(vecIn))
        # Convert the data
        for i in range(len(vecIn)):
            vecOut[i] = np.rint(vecIn[i] * self.table_AIN[gain][0] + self.table_AIN[gain][1])
            vecOut[i] = self.mcclib.volts_USB1608G(gain, np.uint16(vecOut[i]))

        return vecOut, np.linspace(0, timeScan, nScans)

    def getMaxPacketSize(self):
        return self.mcclib.usb_get_max_packet_size(self.dev, 0)

