#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import re

version = ''
with open('mccdaqR/__init__.py', 'r') as fd: version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]',
                                                                 fd.read(), re.MULTILINE).group(1)
if not version:
    raise RuntimeError('Cannot find version information')

requirements = [
    'cffi',
    'numpy',
    'scipy',
]

setup(
    name='mccdaqR',
    version=version,
    description="""mccdaqR is a library to control the Measurement Computing devices""",
    author=['Rachid Azizi'],
    author_email='azi92rach@gmail.com',
    license='Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>',
    url='https://bitbucket.org/azi92rach/mccdaqr.git',
    include_package_data=True,
    packages=find_packages(exclude=['contrib', 'docs', 'tests', 'examples', 'deploy', 'reports']),
    install_requires=requirements,
    package_data={'':[
        '*.h',
        ]
        },
)
