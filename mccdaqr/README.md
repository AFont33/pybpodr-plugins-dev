# README #

## What is this repository for?

Python functions for [Measurement Computing devices](https://mccdaq.com) on linux (tested on Ubuntu).
For the momente it is implemented only for USB-1608GX.

## How to use it

## Installation
First, install de [Linux drivers](https://github.com/wjasper/Linux_Drivers) maintained by Warren J. Jasper.

Install some util libraries:

	sudo apt-get install libudev-dev libusb-1.0-0-dev libfox-1.6-dev
	sudo apt-get install autotools-dev autoconf automake libtool

Install [hidapi](https://github.com/signal11/hidapi):

	git clone https://github.com/signal11/hidapi.git
	cd hidapi
	./bootstrap
	./configure
	make
	sudo make install

Install [drivers](https://github.com/wjasper/Linux_Drivers):

	git clone https://github.com/wjasper/Linux_Drivers.git
	cd Linux_Drivers
	sudo cp 61-mcc.rules /etc/udev/rules.d
	cd USB/mcc-libusb
	make
	sudo make install 
	sudo ldconfig   #(Note, you only need to do this the first time you install the driver.)
	
Then install [mccdaqR](https://bitbucket.org/azi92rach/mccdaqr) library for Python:

	git clone https://bitbucket.org/azi92rach/mccdaqr
	cd mccdaq
	pip install -e .
    
## Examples
Take a look to the Examples folder to have an idea. A documentation will be make when many functions will be developed.

## License
This is Open Source software. We use the `GNU General Public License version 3 <https://www.gnu.org/licenses/gpl.html>`
