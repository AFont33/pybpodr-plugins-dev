# Sound system calibration plugin #

A plugin for [Pyforms Generic Editor Welcome plugin](https://bitbucket.org/fchampalimaud/pybpod-gui-plugin).
It is useful to calibrate sound given the frequency needed.

It is tested on Ubuntu with Python 3.

## How to install:

### Requirements
The plugin uses the [USB-1608GX](https://www.mccdaq.com/usb-data-acquisition/USB-1608G-Series.aspx) data acquisition card. 
To install the drivers and the libraries to control it, install the follows libraries following the instruction on each page:
	
1. Install [toolsR](https://bitbucket.org/azi92rach/toolsr)
2. Install [mccdaqR](https://bitbucket.org/azi92rach/mccdaqr)


### Plugin
1. On your Desktop, create a new folder on your favorite location (e.g. "plugins")
2. Move this plugin folder to that location
3. Open GUI
4. Select Options > Edit user settings
5. Add the following info:

```python
    GENERIC_EDITOR_PLUGINS_PATH = 'PATH TO PLUGINS FOLDER' # USE DOUBLE SLASH FOR PATHS ON WINDOWS
    GENERIC_EDITOR_PLUGINS_LIST = [
        (...other plugins...),
        'sound_calibration_plugin',
    ]
```

6. Save
7. Restart GUI
8. Select Tools and open the plugin
9. If you don't have connected the sound card, don't worry! You can use the plugin for a simulation (NOTE: you are not calibtaring. It is useful just to see the plugin.)

## How to use
The plugin plays the sound N times and records it. There are 2 kind of sound to use, a broadband noise or a pure tone.
It computes the decibels of sound recorded and compares it with the decibels needs within a tolerance. 
This step is ripited till it reaches the goal showing the result. There are 2 possible results:

1. the microphone saturates: it cannot reach the decibels needed. It means that two consecutive 
sound played has the same decibels resulted within the tollerance but it does not reach the goal.
2. the microphone is calibrated: it reached the decibels needed showing the results. 

Moreover it is possible to have a trend of different sound played at the same frequency at different amplitudes and vice versa.
The windows is compose in two parts. The firs one contains 4 settings windows: 

1. **Calibration**: set the variables you need for the calibration.
    - Decibel needs: the decibels you want to reach.
    - Tolerance: the tolerance of the the decibels reached.
    - Number of loop: how many times record the signal before compute the decibels (a mean is used at the end).
    - Speaker's side: you can calibtate left, right or both speakers. 
    ![soundCalibrationPlugin calibration](images/calibration.png)
    
    
2. **DAQ Setttings**: settings of the USB-1608GX.
    - Sample rate: sample rate of the Data acquisition card to use for the recording.
    - Channel: the channel of the Data acquisition card used.
    - Acquisition: duration of recording.
    ![soundCalibrationPlugin daqSettings](images/daqSett.png)
    
    
3. **Micophone Settings**:
    - Gain Microphone: the amplification of the amplifire (if you use it, otherwise it is 0).
    - Sensitivity: sensitivity parameter of the microphone.
    ![soundCalibrationPlugin microphoneSettings](images/micSett.png)
    
    
4. **Sound Card Settings**: 
    - Select the sound card: sound card installed to use for playing the sound.
    - Sample frequency: sample frequency of the sound card (192000Hz for [ASUS Xonar DX](https://www.asus.com/us/Sound-Cards/Xonar_DX/)).
    ![soundCalibrationPlugin soundCardSettings](images/soundcardSett.png)
    
    
The second part contains 4 control windows: 

1. **Broadband Noise**: generate a broadband noise to calibrate the microphone.
    - Starting amplitude: the amplitude used for the first loop.
    - Min/max frequency band: band frequency of noise.
    - Results: the first one is the amplitude to use in your "protocol" while the second is the effective decibels reached.
    - Graph: it shows the threshold of dB needs (red line) and the progress of the calibration (blue line).
    
    *Figure*
    
        - Red: start the calibration and show the results and plot the graph
        - Green: load the past calibrations and plot a general graph
        
    ![soundCalibrationPlugin broadbandcalibration](images/broadbandcalibration.png)
    
    **History**
    
     - Delete: remove the row of data knowing the index.
    
    ![soundCalibrationPlugin boradbandhistory](images/broadbandhistory.png)

2. **Pure tone**: generate a pure tone to calibrate the microphone.
    - Starting amplitude: the amplitude used for the first loop
    - Frequency signal: the frequency of the pure tone.
    - Start button: to run the calibration, and to show the results.
    - Results: the first one is the amplitude to use in your "protocol" while the second is the effective decibels reached.
    - Graph: it shows the threshold of dB needs (red line) and the progress of the calibration (blue line).
    
    *Figure:* 
    
        - Red: start the calibration and show the results and plot the graph
        - Green: load the past calibrations and plot a general graph
    
    ![soundCalibrationPlugin puretonecalibration](images/puretonecalibration.png)
    
    **History**
    
    - Delete: remove the row of data knowing the index.
    
    ![soundCalibrationPlugin puretonehistory](images/puretonehistory.png)
    
3. **Trend Amplitude**: generate a set of amplitude and show the trend increasing it from min to max.
    - Min/Max amplitude: generate a vector of amplitudes from min to max.
    - Iteration: number of values between Min and Max amplitude.
    - Fix frequency: all the sounds are generated with this fix frequency.
    
    *Figure:* 
    
        - Red: Play the sound and show the results and plot the graph
        - Green: load the past results and plot the last one
    
    ![soundCalibrationPlugin amptrend](images/amptrend.png)
    
    **History**
    
    ![soundCalibrationPlugin amptrendhistory](images/amptrendhistory.png)
    
    
4. **Trend Frequency**:
    - Min/Max frequency: generate a vector of frequencies from min to max.
    - Iteration: number of values between Min and Max frequency.
    - Fix amplitude: all the sounds are generated with this fix amplitude.
    
    *Figure:* 
    
        - Red: Play the sound and show the results and plot the graph
        - Green: load the past results and plot the last one
    
    ![soundCalibrationPlugin freqtrend](images/freqtrend.png)
    
    **History**
    
    ![soundCalibrationPlugin freqtrendhistory](images/freqtrendhistory.png)
    

## License
This is Open Source software. We use the `GNU General Public License version 3 <https://www.gnu.org/licenses/gpl.html>`
