# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pyforms import conf
from toolsR import SoundR
import sound_calibration_plugin.sound_utils as sut
import numpy as np, time
import sound_calibration_plugin.settings as setting
import logging
logger = logging.getLogger(__name__)

from AnyQt.QtCore import QTimer, QEventLoop


class TaskFreqTrend:
    """ Run the task of trend frequencie. """
    def __init__(self, windows):
        self.windows = windows # variable to control the variables and methods od sound_windows.py
        # Loop for the task
        self._timerFreqTrend = QTimer()


    def runFreqTrend(self, s, minFreq, maxFreq, nFreq, fixAmp, sens, gainM, FsOut, devOut, FsIn, chDaq, secDaq):
        """ Start the task. """
        self.seconds = s
        self.windows._taskisRunning = True
        self.minFreq = minFreq
        self.maxFreq = maxFreq
        self.nFreq = nFreq
        self.fixAmp = fixAmp
        self.sens = sens
        self.gainM = gainM
        self.FsOut = FsOut
        self.devOut = devOut
        self.FsIn = FsIn
        self.chDaq = chDaq
        self.secDaq = secDaq
        # Start
        # Initialization variables
        self.result_db = 0
        self.last_result_db = 0
        self.counterLoop = 0 # counter to use instead using for
        self._stop = False # Flag
        self.runTask(True) # Start the task
        self._timerFreqTrend.timeout.connect(self.runTask)
        if not self._stop:
            self._timerFreqTrend.start(setting.SOUND_PLUGIN_REFRESH_RATE) # Run the loop

    def runTask(self, startP = False):
        try:

            if startP:
                # Create a logscale frequencies
                idx = np.log10(self.maxFreq / self.minFreq) / (self.nFreq - 1)
                self.windows.vecFreq = np.array([self.minFreq * 10 ** (i * idx) for i in range(self.nFreq)]).astype(int)
                # Create the sound server
                self.soundStream = SoundR(sampleRate=self.FsOut, deviceOut=self.devOut, channelsOut=2)
                # Progress bar
                self.windows._progress.value = 0
                self.windows._progress.max = len(self.windows.vecFreq)
                self.windows._progress.show()

            if self.counterLoop < len(self.windows.vecFreq):
                        self.windows._progress += 1
                        # Generate your sound arrays
                        s1, s_zeros = sut.genPureTone(self.seconds, self.FsOut, self.windows.vecFreq[self.counterLoop], self.fixAmp)

                        # Play the sound
                        if self.windows._side.value == 0:  # both sides
                            self.soundStream.load(s1)
                        elif self.windows._side.value == 1:  # left speaker
                            self.soundStream.load(s1, s_zeros)
                        elif self.windows._side.value == 2:  # right speaker
                            self.soundStream.load(s_zeros, s1)
                        self.soundStream.playSound()

                        # Record the signal by mmcdaqR and save it
                        if setting.SOUND_WITHOUT_DAQ: # <-- if SOUND_WITHOUT_DAQ = 1
                            data = s1
                            time.sleep(1)
                        else:                           # <-- if SOUND_WITHOUT_DAQ = 0
                            data, vTime = sut.scanSignal(self.chDaq, self.secDaq, self.FsIn,
                                                         self.sens)

                        # Stop the sound
                        self.soundStream.stopSound()
                        time.sleep(0.1)

                        # Compute the db and add it to the results
                        if setting.SOUND_WITHOUT_DAQ: # <-- if SOUND_WITHOUT_DAQ = 1
                            self.result_db = sut.pureTone_db([data], self.windows.vecFreq[self.counterLoop],
                                                        self.FsOut, gain_michrophone=self.gainM)
                        else:                           # <-- if SOUND_WITHOUT_DAQ = 0
                            self.result_db = sut.pureTone_db([data], self.windows.vecFreq[self.counterLoop],
                                                        self.FsIn, gain_michrophone=self.gainM)
                        self.windows._listResults.append(self.result_db)
                        self.counterLoop += 1
            else:
                        # Save all the data and plot the results
                        self.windows.myData.saveTrendFreq(self.fixAmp, self.windows.vecFreq, self.windows._listResults,
                                                          self.windows._side.text, self.windows._devOut.text)
                        self.windows.mode = 3
                        self.windows._graph.value = self.windows._on_draw
                        self._timerFreqTrend.stop()
                        self._stop = True
                        self.windows._taskisRunning = False
                        self.windows._progress.hide()

            QEventLoop()

        except Exception as err: # Catch the errors.
            self._timerFreqTrend.stop()
            self._stop = True
            self.windows._taskisRunning = False
            self.windows._progress.hide()
            logger.error(str(err), exc_info=True)
            self.windows.critical("Unexpected error while running sound calibration. Pleas see log for more details.\n"
                                 "E: task_freqtrend", "Error")
