# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pyforms import conf
from toolsR import SoundR
import sound_calibration_plugin.sound_utils as sut
import numpy as np, time
import sound_calibration_plugin.settings as setting
import logging
logger = logging.getLogger(__name__)

from AnyQt.QtCore import QTimer, QEventLoop


class TaskAmpTrend:
    """ Run the task of trend amplitudes. """
    def __init__(self, windows):
        self.windows = windows # variable to control the variables and methods od sound_windows.py
        # Loop for the task
        self._timerAmpTrend = QTimer()
        self._stop = False  # Flag


    def runAmpTrend(self, s, minAmp, maxAmp, nAmp, fixFreq, sens, gainM, FsOut, devOut, FsIn, chDaq, secDaq):
        """ Start the task. """
        self.seconds = s
        self.windows._taskisRunning = True
        self.minAmp = minAmp
        self.maxAmp = maxAmp
        self.nAmp = nAmp
        self.fixFreq = fixFreq
        self.sens = sens
        self.gainM = gainM
        self.FsOut = FsOut
        self.devOut = devOut
        self.FsIn = FsIn
        self.chDaq = chDaq
        self.secDaq = secDaq
        # Start
        # Initialization variables
        self.result_db = 0
        self.last_result_db = 0
        self.counterLoop = 0 # counter to use instead using for
        self._stop = False # Flag
        self.runTask(True) # Start the task
        self._timerAmpTrend.timeout.connect(self.runTask)
        if not self._stop:
            self._timerAmpTrend.start(setting.SOUND_PLUGIN_REFRESH_RATE) # Run the loop

    def runTask(self, startP = False):
        try:

            if startP:
                # Create a logscale frequencies
                idx = np.log10(self.maxAmp / self.minAmp) / (self.nAmp - 1)
                self.windows.vecAmp = np.array([self.minAmp * 10 ** (i * idx) for i in range(self.nAmp)])
                # Create the sound server
                self.soundStream = SoundR(sampleRate=self.FsOut, deviceOut=self.devOut, channelsOut=2)
                # Progress bar
                self.windows._progress.value = 0
                self.windows._progress.max = len(self.windows.vecAmp)
                self.windows._progress.show()

            if self.counterLoop < len(self.windows.vecAmp):
                        self.windows._progress += 1
                        # Generate your sound arrays
                        s1, s_zeros = sut.genPureTone(self.seconds, self.FsOut, self.fixFreq, self.windows.vecAmp[self.counterLoop])

                        # Play the sound
                        if self.windows._side.value == 0: # both sides
                            self.soundStream.load(s1)
                        elif self.windows._side.value == 1: # left speaker
                            self.soundStream.load(s1, s_zeros)
                        elif self.windows._side.value == 2: # right speaker
                            self.soundStream.load(s_zeros, s1)
                        self.soundStream.playSound()

                        # Record the signal by mmcdaqR and save it
                        if setting.SOUND_WITHOUT_DAQ: # <-- if SOUND_WITHOUT_DAQ = 1
                            data = s1
                            time.sleep(1)
                        else:                           # <-- if SOUND_WITHOUT_DAQ = 1
                            data, vTime = sut.scanSignal(self.chDaq, self.secDaq, self.FsIn,
                                                         self.sens)

                        # Stop the sound
                        self.soundStream.stopSound()
                        time.sleep(0.1)

                        # Compute the db and add it to the results
                        if setting.SOUND_WITHOUT_DAQ:
                            self.result_db = sut.pureTone_db([data], self.fixFreq, self.FsOut, gain_michrophone=self.gainM)  # <-- TEST MAC
                        else:
                            self.result_db = sut.pureTone_db([data], self.fixFreq, self.FsIn, gain_michrophone=self.gainM)  # <-- TEST ubuntu
                        self.windows._listResults.append(self.result_db)
                        self.counterLoop += 1
            else:
                        # Save all the data and plot the results
                        self.windows.myData.saveTrendAmp(self.fixFreq, self.windows.vecAmp, self.windows._listResults,
                                                         self.windows._side.text, self.windows._devOut.text)
                        self.windows.mode = 2
                        self.windows._graph.value = self.windows._on_draw
                        self._timerAmpTrend.stop()
                        self._stop = True
                        self.windows._taskisRunning = False
                        self.windows._progress.hide()

            QEventLoop()

        except Exception as err: # Catch the errors
            self._timerAmpTrend.stop()
            self._stop = True
            self.windows._taskisRunning = False
            self.windows._progress.hide()
            logger.error(str(err), exc_info=True)
            self.windows.critical("Unexpected error while running sound calibration. Pleas see log for more details."
                                 "E: task_amptrend", "Error")
