import csv
import logging
import os
from pathlib import Path

class Logbook:
    """
    This class manages the calibration log for the Arduino devices.
    """

    def __init__(self, path, filename):
        self._path = path
        self._filename = filename
    
    def save(self, data, header):
        # Check the folder status:
        self._manage_directories()
        existing_record = Path(os.path.join(self._path, self._filename))
        if existing_record.exists():
            with open( os.path.join(self._path, self._filename), "a+") as log:
                record = csv.writer(log, delimiter=';')
                record.writerow(data)
        else:
            # If the file doesn't yet exist, create it with the header:
            with open( os.path.join(self._path, self._filename), "w+") as log:
                record = csv.writer(log, delimiter=';')
                record.writerow(header)
                record.writerow(data)

    def _manage_directories(self):    
        """ 
        Creates the log folder the first time that the program is executed.
        """        
        if not os.path.exists(self._path): 
            os.makedirs(self._path)
            logging.info("Log directory not found. Creating it...")        