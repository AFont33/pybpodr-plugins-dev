# toolsR #

## About

### What is toolsR?

This repository gives methods to control a USB camera and audio using Python.

### Classes
 
 - VideoR controls the camera, records and shows the streaming in a windows.
 - VideoRP works as VideoR but is used if you are using Qt.
 - SoundR: controls the sound card and the triggering sound.
 
## Requirements:

  - [OpenCV](http://opencv.org/) > 3.x (tested on 3.2.0) 
  - [Sounddevice](https://github.com/spatialaudio/python-sounddevice.git) library
  
**Warning**: the toolsR library contains also library about Video recording that uses [OpenCV](http://opencv.org/). If you need only the sound for your project and you have not OpenCV installed, my suggestion is to modify the file toolsr/toolsR/\_\_init\_\_.py and remove all the import about the video.


## HOW TO INSTALL

Inside the main folder use the command:
    
    git clone https://bitbucket.org/azi92rach/toolsr
    cd toolsr
    pip install -e .

### Low latency kernel (only if you need low latency)

Update:
	
	sudo apt-get update
	sudo apt-get install linux-lowlatency linux-headers-lowlatency

Open the file Grub

	sudo gedit /etc/default/grub

Commented the line GRUB_HIDDEN_TIMEOUT as follows:

	GRUB_HIDDEN_TIMEOUT=0

It is possible to select the lowlatency kernel as default, setting the line GRUB_DEFAULT as foolwos:

	GRUB_DEFAULT="Advanced options for Ubuntu>Ubuntu, with Linux 4.4.0-92-lowlatency"

Save and exit.
Update the Grub
	
	sudo update-grub

Open the file /etc/security/limits.conf as administrator

	sudo gedit /etc/security/limits.conf

Insert before the last row 'End of file' the following strings:

	@realtime   -  rtprio     99
	@realtime   -  memlock    unlimited

Save and exit.
Add the user to the realtime group

	sudo groupadd realtime
	sudo usermod -a -G realtime $USER
    
## DOCUMENTATION

### Video:

    video = toolsR.VideoR(idx_camera = 0, PARAMETERS)
                                    
    video = toolsR.VideoRP(idx_camera = 0, PARAMETERS) # if you need to run it with popen 
    
    PARAMETERS:
    name_video: [STRING] Name of the video. IMPORTANT: the container has to be included included (e.g. 'name_video.mkv')
        default = date 
        
    path: [STRING] Where to save the video
        default = path where are you running the code 
        
    width, height: [INTEGER, INTEGER] Width and height of the frames
        default = defaulta values from camera
        
    fps: [INTEGER] Frame rate
        default = default value from camera
        
    codec_camera: [STRING] codec camera. (Be sure that the camera support it)
        default = 'MJPG'
        
    codec_video: [STRING] codec used to record the video
        default = 'MJPG'
        
    showWindows:  [BOOLEAN] If True, a windows shows the streaming. 
        default = True
    
    METHODS:
    play(): play the streaming
    record(): start record, or continue to record after a pause.
    pause(): pause the video
    stop(): stop the streaming and save the video
    get_fps(): return the camera frame rate (Only for VideoR)
    get_size(): return the [width, length] of the frames (Only for VideoR)
    get_codec(): return the codec of the camera (Only for VideoR)

### Video Tracking 
The video tracking uses the same methods of videoR. When you call record() you will draw some boxes.
The tracking system sends softcodes with the index following the order which you created the boxes.

    video = toolsR.VideoR(idx_camera = 0, tracking = True, PARAMETERS)
    
    PARAMETERS:
     -> The same of the Video, and...
     
     index_camera: [INTEGER or STRING] can be the camera index or the path of your video
     nAreas: [INTEGER] The number of boxes for detection
     bpod_address: [INTEGER] the bpod address. 
     
     METHODS:
     The same of the video while here when you call record() you will draw the boxes.
   
### Audio:
 
    sound = toolsR.SoundR(sampleRate = 44100, PARAMETERS)   
    
    PARAMETERS:
    deviceOut: [INTEGER] Device index(es) or query string(s) specifying the device(s) to be used. Check from the terminal your device index using SoundR.getDevices()
        default = 'default'
        
    channelsOut: [INTEGER] The number of channels of sound to be delivered to the stream
        default = 2
    
    lowLatency: [float or {‘low’, ‘high’}] – The desired latency in seconds. The special values 'low' and 'high' (latter being the default) select the default low and high latency. 
        default = 'low'
        
    METHODS:
    load(sound): load the sound every time before use playSound()
    playSound(): play the latest sound loaded in background
    stopSound(): stop the sound played
    
## HOW TO USE IT?

There are some python example into the Examples folder. Take a look to test it.  


    
## License
This is Open Source software. We use the `GNU General Public License version 3 <https://www.gnu.org/licenses/gpl.html>`
