#!/usr/bin/python3
# -*- coding: utf-8 -*-

import time
from toolsR import VideoR as vr

# TEST VIDEO
def start():
    video = vr(0, path="/home/parallels/Videos/", fps=60) # -------------- create the folder video
    video.play()
    video.record()
    time.sleep(10)
    video.stop()
    return


if __name__ == '__main__':
    start()

