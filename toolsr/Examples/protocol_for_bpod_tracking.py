# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pybpodapi.protocol import Bpod, StateMachine
from pybpodapi.bpod.hardware.output_channels import OutputChannel
from toolsR import VideoR as vr
my_bpod = Bpod()


video = vr(0, path="/home/", fps=60, tracking = True, bpod_address = 36000)
video.play()
video.record()

for i in range(2):
    sma = StateMachine(my_bpod)

    sma.add_state(
	    state_name='State0',  # Trigger global timer
	    state_timer=0,
	    state_change_conditions={Bpod.Events.Tup: 'Waiting'},
	    output_actions=[])

    sma.add_state(
	    state_name='Waiting',  # Infinite loop (with next state). Only a global timer can save us.
	    state_timer=5,
	    state_change_conditions={'SoftCode1': 'OpenValve1', 'SoftCode2': 'Light1', 'SoftCode3': 'exit', Bpod.Events.Port1In: 'exit'},
	    output_actions=[])

    sma.add_state(
	    state_name='OpenValve1',
	    state_timer=0.1,
	    state_change_conditions={Bpod.Events.Tup: 'State0'},
	    output_actions=[(OutputChannel.Valve, 1)])

    sma.add_state(
	    state_name='Light1',
	    state_timer=0.1,
	    state_change_conditions={Bpod.Events.Tup: 'State0'},
	    output_actions=[(Bpod.OutputChannels.PWM1, 255)])

    my_bpod.send_state_machine(sma)

    my_bpod.run_state_machine(sma)

    print("Current trial info: {0}".format(my_bpod.session.current_trial) )

video.stop()
my_bpod.close()