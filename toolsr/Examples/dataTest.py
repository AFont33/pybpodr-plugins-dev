# !/usr/bin/python3
# -*- coding: utf-8 -*-

from toolsR import  DataR


PATH = '' # <----- where to save the file

# Create the structure
d = DataR(path = PATH, name = 'test')

# Some values to use as test
aa = {'States timestamps': {'FlashStimulus': [(1.0, 1.1)], 'Punish': [(13.1776, 16.1776)],
                            'WaitForResponse': [(1.1, 13.1776)], 'Reward': [('nan','nan')],
                            'WaitForPort2Poke': [(0, 1.0)]},
      'Events timestamps': {'Port1Out': [13.402], 'Tup': [1.0, 1.1, 16.1776], 'Port1In': [13.1776]},
      'Bpod start timestamp': 0.119}
bb = {'States timestamps': {'FlashStimulus': [(1.0, 1.1)], 'Punish': [('nan', 'nan')],
                            'WaitForResponse': [(1.1, 4.3894)],'Reward': [(4.3894, 4.4894)],
                            'WaitForPort2Poke': [(0, 1.0)]},
      'Events timestamps': {'Tup': [1.0, 1.1, 4.4894], 'Port1In': [4.3894]},
      'Bpod start timestamp': 16.303}

# Add the value to the structure
d.add(aa)
d.add(bb)

# Save the structure as json file
d.save()

# Now open the file and print the results
d = DataR() # To be sure that we are not using the previous data we overlap the object
print('------------------------ READ ------------------------------')
d.open('test')
print(d.getStates())
print(d.getEvents())
print(d.getBpodTimestamps())



